/*
 * libre_register_server.vala
 *
 * Copyright 2021 John Toohey <john_t@mailo.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */
using LrDB, LrTB;
const string help = 
"""
libre-register-tool

Usage: libre-register-tool {create|run|modify} [file]

	create  - runs through a setup wizard.
	modify  - modifies a database.          //TODO*: Implement
	run     - runs a database.              //TODO*: Implement

	run:
		--port [port] - the port to run on  //TODO*: Implement

""";
/// The main class
public class Main : Object {

    // The main function
    static int main(string[] args) {
        switch (args[1]) {
    		case ("create"):
				create_db(args);
        		break;
        	case ("modify"):
	        	modify_db(args);
	        	break;
			case ("--help"):
    		case ("help"):
	    	default:
    			stdout.printf(help);
    			break;
        }
    	return 0;
    }
    // Setup Sequence
    static void create_db(string[] args) {
	    string filepath = get_file_from_args(args);
		// Begins Creation process
		if (filepath!=null) {
			stdout.printf("\033[92mStarting Database Creation Process\033[0m\n");

			string database_name = 	query_user_string("What is the name of the school:", "name");
			string database_id =	query_user_string("What should the id of the school be:", "id");
			LrDB.DataBase db = new LrDB.DataBase(database_name, database_id);

			stdout.printf("\033[92mCreated Database\033[0m\n");
			bool to_write = query_user_boolean("Would you like to write to a file now?");
			bool to_loop = true;
			while (to_loop) {
				if (to_write) {
					try {
						to_loop = false;
						// Writes to a new file
						stdout.printf("\033[92mWriting To File\033[0m\n");
						stdout.printf("This may take some time\n");
						var file = File.parse_name(filepath);
						if (file.query_exists())
							file.delete();
						db.write_to_new_file(filepath);
						stdout.printf("\n\033[92mCompleted Write\033[0m\n");
					} catch (Error e) {
						print_error(e);
					}
				}
				else {
					to_loop = !query_user_boolean("Are you sure you want to exit?");
					to_write=true;
				}
			}
		}
    }
    static void modify_db(string[] args) {
		string filepath = get_file_from_args(args);
		if (filepath != null) {

		}
    }
    static string? get_file_from_args(string[] args) {
		string? filepath = null;
		bool to_create = false;
		
		// If a file is specified
		if (args.length>=3)
			filepath = args[2];
		// If a file is not specified
		else {
			stdout.printf("You have not selected a file\n");
			stdout.printf("\033[93m::\033[0m Please enter your file path: ");
			filepath = stdin.read_line();
		}
		{
    		// Checks if the file exists
    		var file = File.parse_name(filepath);
			if (file.query_exists())
    			stdout.printf("\033[96mCaution! File exists.\033[0m\n");
		}
		to_create = query_user_boolean(@"Are you sure that \033[93m\"$filepath\"\033[0m is the correct file path?");
		if (to_create) {
			return filepath;
		}
		return null;
    }
    static bool query_user_boolean(string query) {
	    string prep_query = query.chomp().chug();
		stdout.printf(@"\033[96m::\033[0m $prep_query\033[0m \033[32m[Y/n]\033[0m");
		// Confirms the user's choice
		string response = stdin.read_line();
		if (response[0].toupper()=='Y'||
			response.length==0)
			return true;
		return false;
    }
    static string query_user_string(string query, string query_name) {
		bool is_happy = false;
		string result = "";
		// School name
		while (!is_happy) {
			stdout.printf(@"\033[93m::\033[0m $query \033[0m");
			result = stdin.read_line();
			is_happy = query_user_boolean(@"Is \033[93m\"$result\"\033[0m the correct $query_name?");
		}
		return result;
    }
    static void print_error(Error e) {
		stdout.printf("\033[91m An Error Has Occured\n\033[31m%s\033[0m".printf(e.message));
    }
}
